import parsetoml
import strutils
import telebot, asyncdispatch, logging, options
import os
import times

proc toInt(s: string): int =
  return (parseInt(s.split(":")[0]) * 60) + parseInt(s.split(":")[1])

# Load config file
let data = parsetoml.parseFile("config.toml")

# Load vars
let userName = data["name"].getStr()
let dayEnd = toInt(data["ends"].getStr())

# Telegram API key from enviroment variable
const API_KEY = getEnv("TIMETABLE_KEY")

# Function that handles incoming messages
proc updateHandler(b: Telebot, u: Update) {.async.} =
  var response = u.message.get
  if response.text.isSome:

    # Output incoming message to terminal
    echo "---"

    # Get day
    let day = getTime().local.weekday
    const days = @ ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    let dayNow = days[ord(day)]
    echo dayNow

    # Get time in minutes since midnight
    let minutesNow = ((getTime().local.hour) * 60) + getTime().local.minute
    echo minutesNow

    var text = "Unknown command :/" # Default to unknown command

    # If finding next class
    if response.text.get.contains("next"):

      # Check if past end of day
      let tl = data[dayNow]["times"].getElems()
      if minutesNow >= toInt(tl[tl.len()-1].getStr()):
        text = "Nothing!"

      else:
        # Iterate over periods and find next
        var index = 0
        for i in data[dayNow]["times"].getElems():
          if toInt(i.getStr()) >= minutesNow:
            text = "Your next period is " & data[dayNow]["titles"].getElems()[index].getStr()
            break
          index += 1

    # If finding todays classes
    elif response.text.get.contains("today"):
      echo "Today!"

      # Iterate over periods and list
      text = ""
      var index = 0
      for i in data[dayNow]["times"].getElems():
        text = text & "\n" & capitalizeAscii(data[dayNow]["titles"].getElems()[index].getStr()) & ": " & i.getStr()
        index += 1

    var message = newMessage(response.chat.id, text)
    message.disableNotification = true
    message.parseMode = "markdown"
    discard await b.send(message)

let bot = newTeleBot(API_KEY)
bot.onUpdate(updateHandler)
bot.poll(timeout=300)
